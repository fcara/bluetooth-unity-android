﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using ServerClient;

public class CommandHandler : MonoBehaviour
{

	public delegate void GetCommand(SmartphoneCommand command);
	public event GetCommand receptionConfirmed;
	public event GetCommand onReset;

	public DataClient dataClient;
	public SmartphoneCommand.Modes startMode = SmartphoneCommand.Modes.NoSelection;

	public bool debug = false;

	private SmartphoneCommand currentState;

	// Use this for initialization
	void Start()
	{
		dataClient.getDataDelegate += getData;
		currentState = new SmartphoneCommand();
		currentState.mode = startMode;
		changeMenu();
	}
	
	// Update is called once per frame
	void Update()
	{

	}

	void OnGUI()
	{
		if (debug)
		{
			GUI.Label(new Rect(10, 10, 500, 20), currentState.ToString());
		}
	}



	public void sendCommand(SmartphoneCommand command)
	{
		dataClient.sendData(SerializerTool.SerializeObject<SmartphoneCommand>(command));
	}

	public void confirmReception(SmartphoneCommand command)
	{
		command.commandType = SmartphoneCommand.Commands.ConfirmReception;
		sendCommand(command);
	}

	public string getConnectionStatus()
	{
		return dataClient.getConnectionState();
	}

	public void changeMode(SmartphoneCommand.Modes mode)
	{
		currentState.mode = mode;
		changeMenu();
	}



	// Code snippet for updating a menu based on the commands
	private void changeMenu()
	{
		try
		{
			/*List<string> newList = new List<string>();
			foreach (Tuple<string, SmartphoneCommand.Commands, SmartphoneCommand.Modes, 
			         SmartphoneCommand.States> t in currentState.commandsForCurrentMode())
			{
				newList.Add(t.First);
			}
			menu.MenuElements = newList;

			widgetManager.CurrentMode = currentState.mode;*/
		}
		catch (NullReferenceException)
		{
			Debug.LogError("NullReferenceException: did you add a Mode to SmartphoneCommand " +
				"without populating the list of commands for that mode?");
		}
	}

	private void getData(string data)
	{
		SmartphoneCommand command = SerializerTool.DeserializeObject<SmartphoneCommand>(data);

		if (command.commandType == SmartphoneCommand.Commands.ConfirmReception)
		{
			if (receptionConfirmed != null)
			{
				receptionConfirmed.Invoke(command);
			}
			return;
		}
		else if (command.commandType == SmartphoneCommand.Commands.Reset)
		{
			if (onReset != null)
			{
				onReset.Invoke(command);
			}
			dataClient.sendData(data); // Confirm reception of Reset by sending it back
			return;
		}

		currentState = command;
		changeMenu();
	}
}

















