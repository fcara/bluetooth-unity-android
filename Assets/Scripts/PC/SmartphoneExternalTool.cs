﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ServerClient;
using ServerClient.TCP;

public class SmartphoneExternalTool : MonoBehaviour
{

	public DataServer dataServer;

	public delegate void SmartphoneInput(SmartphoneCommand command);
	public delegate void SmartphoneCommandDelegate(SmartphoneCommand command);

	// onInput will be called whenever a Command is received
	public event SmartphoneInput onInput;

	private SmartphoneCommandDelegate[] onCommand;

	void Awake()
	{
		onCommand = new SmartphoneCommandDelegate[(int)SmartphoneCommand.Commands.Count];
	}

	// Use this for initialization
	void Start()
	{
		dataServer.getDataDelegate += getData;

		// test
		//registerOnCommand(SmartphoneCommand.Commands.Yes, test);
	}
	
	// Update is called once per frame
	void Update()
	{
		// test
		/*if (Input.GetKeyDown(KeyCode.Space))
		{
			SmartphoneCommand c = new SmartphoneCommand();
			c.commandType = SmartphoneCommand.Commands.No;
			c.text = "this is a test";
			Debug.Log("Sending No command with text=" + c.text);
			sendCommand(c);
		}*/
	}



	// callback will be called only when a Command of type command is received
	public void registerOnCommand(SmartphoneCommand.Commands command, SmartphoneCommandDelegate callback)
	{
		onCommand[(int)command] += callback;
	}

	public void sendCommand(SmartphoneCommand command)
	{
		dataServer.sendData(SerializerTool.SerializeObject<SmartphoneCommand>(command));
	}

	public void confirmReception(SmartphoneCommand command, int returnValue = 0)
	{
		command.commandType = SmartphoneCommand.Commands.ConfirmReception;
		command.returnValue = returnValue;
		sendCommand(command);
	}



	private void getData(string data)
	{
		SmartphoneCommand command = SerializerTool.DeserializeObject<SmartphoneCommand>(data);

		// test
		//Debug.Log("Received from smartphone " + command.ToString());
		//Debug.Log(command.text + " | " + command.color.ToString());
		//Debug.Log(command.getCommandString());

		// Generic callback
		if (onInput != null)
			onInput.Invoke(command);

		int commandValue = (int)command.commandType;
		if (commandValue < (int)SmartphoneCommand.Commands.Count)
		{
			// Specific callback
			if (onCommand[commandValue] != null)
				onCommand[commandValue].Invoke(command);
		}
	}

	// test
	private void test(SmartphoneCommand command)
	{
		Debug.Log("Received Yes command with text=" + command.text);
	}
}














