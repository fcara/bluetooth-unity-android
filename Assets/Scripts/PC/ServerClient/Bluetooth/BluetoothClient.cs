﻿using UnityEngine;
using System.Collections;

namespace ServerClient.Bluetooth
{
	public class BluetoothClient : GenericClient
	{

		private string data = "";
		private float time = 0.0f;
		private string lastConnectionState = "Bluetooth Transmitter";

		// Use this for initialization
		public override void Start(string gameObjectName)
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			BluetoothPluginBridge.SetupBluetoothTransmitter(gameObjectName, "clientGetDataFromObjectStart",
			                                                "clientGetDataFromObject", "clientGetDataFromObjectEnd");
			BluetoothPluginBridge.StartClientListening();
#endif
		}
		
		// Update is called once per frame
		public override void Update()
		{
			time += Time.deltaTime;
			if (time >= 0.1f)
			{
				time = 0.0f;
#if UNITY_ANDROID && !UNITY_EDITOR
				lastConnectionState = BluetoothPluginBridge.GetConnectionState();
#endif
			}
		}

		public override void OnDestroy()
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			BluetoothPluginBridge.StopClientListening();
#endif
		}

		public override string getConnectionState()
		{
			return lastConnectionState;
		}
		
		public override void sendData(string data)
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			BluetoothPluginBridge.SendData(data);
#endif
		}

		public override void clientGetDataFromObjectStart(string data)
		{
			this.data = "";
		}

		public override void clientGetDataFromObject(string data)
		{
			this.data += data;
		}

		public override void clientGetDataFromObjectEnd(string data)
		{
			sendDataToDelegate(this.data);
		}
	}
}














