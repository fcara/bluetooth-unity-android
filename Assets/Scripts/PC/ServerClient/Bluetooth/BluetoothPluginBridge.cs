﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace ServerClient.Bluetooth
{
	public class BluetoothPluginBridge
	{
		public static string ReturnString()
		{
			AndroidJavaClass ajc = new AndroidJavaClass("ca.etsmtl.aj47620.bluetoothplugin.Bridge");
			return ajc.CallStatic<string>("ReturnString");
		}

		public static int ReturnInt()
		{
			AndroidJavaClass ajc = new AndroidJavaClass("ca.etsmtl.aj47620.bluetoothplugin.Bridge");
			return ajc.CallStatic<int>("ReturnInt");
		}

		public static string ReturnInstanceString()
		{
			AndroidJavaObject ajc = new AndroidJavaObject("ca.etsmtl.aj47620.bluetoothplugin.Bridge");
			return ajc.Call<string>("ReturnInstanceString");
		}
		
		public static int ReturnInstanceInt()
		{
			AndroidJavaObject ajc = new AndroidJavaObject("ca.etsmtl.aj47620.bluetoothplugin.Bridge");
			return ajc.Call<int>("ReturnInstanceInt");
		}




		private static AndroidJavaObject bridge = null;

		public static void SetupBluetoothTransmitter(string gameObjectName, string startMethodName, 
		                                             string dataMethodName, string endMethodName)
		{
			bridge = new AndroidJavaObject("ca.etsmtl.aj47620.bluetoothplugin.Bridge");
			bridge.Call("SetupBluetoothTransmitter", gameObjectName, startMethodName, dataMethodName, endMethodName);
		}

		public static bool StartClientListening()
		{
			if (bridge == null)
			{
				return false;
			}
			bridge.Call("StartClientListening");
			return true;
		}

		public static bool SendData(string data)
		{
			if (bridge == null)
			{
				return false;
			}
			bridge.Call("SendData", data);
			return true;
		}

		public static bool StopClientListening()
		{
			if (bridge == null)
			{
				return false;
			}
			bridge.Call("StopClientListening");
			return true;
		}

		public static string GetConnectionState()
		{
			if (bridge == null)
			{
				return "Bluetooth not initialized";
			}
			return bridge.Call<string>("GetConnectionState");
		}
	}
}















