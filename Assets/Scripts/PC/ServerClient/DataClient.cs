﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ServerClient;
using ServerClient.Bluetooth;
using ServerClient.TCP;

namespace ServerClient
{
	public class DataClient : MonoBehaviour
	{

		public enum Types
		{
			TCP = 0,
			Bluetooth
		};

		public Types TransmissionType = Types.Bluetooth;
		public event GenericClient.GetData getDataDelegate
		{
			add
			{
				client.getDataDelegate += value;
			}
			remove
			{
				client.getDataDelegate -= value;
			}
		}

		private GenericClient client;

		// Use this for initialization
		void Awake()
		{
			if (TransmissionType == Types.Bluetooth)
			{
				client = new BluetoothClient();
			}
			else if (TransmissionType == Types.TCP)
			{
				client = new TCPClient();
			}
			else
			{
				Debug.LogError("Unsupported transmission type!");
			}
			client.Start(this.gameObject.name);
		}
		
		// Update is called once per frame
		void Update()
		{
			client.Update();
		}

		void OnDestroy()
		{
			client.OnDestroy();
		}

		public string getConnectionState()
		{
			return client.getConnectionState();
		}

		public void sendData(string data)
		{
			client.sendData(data);
		}

		public string setIP(string ipText)
		{
			return client.setIP(ipText);
		}

		public string restartAutomaticIP()
		{
			return client.restartAutomaticIP();
		}

		public string switchTransmissionType()
		{
			string ret = "";

			if (TransmissionType == Types.Bluetooth)
			{
				client.OnDestroy();
				TransmissionType = Types.TCP;
				client = new TCPClient();
				ret = "Switching to Wifi";
			}
			else
			{
				client.OnDestroy();
				TransmissionType = Types.Bluetooth;
				client = new BluetoothClient();
				ret = "Switching to Bluetooth";
			}
			client.Start(this.gameObject.name);

			return ret;
		}

		// These methods are for receiving asynchronous data from eventual plugins
		public void clientGetDataFromObjectStart(string data)
		{
			client.clientGetDataFromObjectStart(data);
		}
		
		public void clientGetDataFromObject(string data)
		{
			client.clientGetDataFromObject(data);
		}
		
		public void clientGetDataFromObjectEnd(string data)
		{
			client.clientGetDataFromObjectEnd(data);
		}
	}
}












