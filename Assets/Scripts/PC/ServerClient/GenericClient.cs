﻿using System.Collections;
using System.Collections.Generic;

namespace ServerClient
{
	public abstract class GenericClient
	{

		public delegate void GetData(string data);
		public event GetData getDataDelegate;

		// Use this for initialization
		public abstract void Start(string gameObjectName);
		
		// Update is called once per frame
		public abstract void Update();

		public abstract void OnDestroy();

		public virtual string getConnectionState()
		{
			return "Uninitialized";
		}
		
		public abstract void sendData(string data);
		
		public virtual string setIP(string ipText)
		{
			return "";
		}
		
		public virtual string restartAutomaticIP()
		{
			return "";
		}

		public void sendDataToDelegate(string data)
		{
			getDataDelegate(data);
		}

		public virtual void clientGetDataFromObjectStart(string data)
		{

		}
		
		public virtual void clientGetDataFromObject(string data)
		{

		}
		
		public virtual void clientGetDataFromObjectEnd(string data)
		{

		}
	}
}













