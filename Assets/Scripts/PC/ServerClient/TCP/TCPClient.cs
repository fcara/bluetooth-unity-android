﻿using System.Collections;
using System.Collections.Generic;

namespace ServerClient.TCP
{
	public class TCPClient : GenericClient
	{

		private LockFreeLinkPool<string> dataIn;
		
		// Use this for initialization
		public override void Start(string gameObjectName)
		{
			dataIn = new LockFreeLinkPool<string>();
			TransmitterSingleton.Instance.clientStartListening(new TransmitterSingleton.GetData(clientGetData));
		}
		
		// Update is called once per frame
		public override void Update()
		{
			// Check Concurrent Pool for data
			List<string> dataList = new List<string>();
			SingleLinkNode<string> node = null;
			while (dataIn.Pop(out node))
			{
				dataList.Add(node.Item);
			}
			if (dataList.Count > 0)
			{
				// Turn the LIFO list into a FIFO list
				dataList.Reverse();
				foreach (string data in dataList)
				{
					sendDataToDelegate(data);
				}
			}
		}
		
		public override void OnDestroy()
		{
			TransmitterSingleton.Instance.clientStopListening();
		}

		public override string getConnectionState()
		{
			return TransmitterSingleton.Instance.connectionState;
		}
		
		public override void sendData(string data)
		{
			TransmitterSingleton.Instance.sendDataToServer(data);
		}
		
		public override string setIP(string ipText)
		{
			string ret = "";
			
			if (!TransmitterSingleton.Instance.isValidIPAddress(ipText))
			{
				ret = "Invalid IP Address";
			}
			else if (!TransmitterSingleton.Instance.restartClientListening(new TransmitterSingleton.GetData(clientGetData), ipText))
			{
				ret = "Already restarting communications";
			}
			
			return ret;
		}
		
		public override string restartAutomaticIP()
		{
			if (!TransmitterSingleton.Instance.restartClientListening(new TransmitterSingleton.GetData(clientGetData)))
			{
				return "Already restarting communications";
			}
			
			return "";
		}
		
		public void clientGetData(string data)
		{
			// In order to get back to the main thread
			// (Unity cannot handle scene modifications on other threads),
			// we put the data in a concurrent pool
			SingleLinkNode<string> node = new SingleLinkNode<string>();
			node.Item = data;
			dataIn.Push(node);
			// Next part of the pipeline is DataHandler.Update()
		}
	}
}













